package javax.xianfeng.antdpro.model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.hello123.model.demo.User;

/**
 * @since 2020/01/27 22:59:27
 */
public class PagingApiResultTest {
	
	@Test
	public void toJsonObject() {
		PagingApiResult result = new PagingApiResult();
		Paging paging = new Paging();
		result.setPagination(paging);
		List<User> list = new ArrayList<>();
		User user = new User();
		user.setId(1001L);
		list.add(user);
		result.setList(list);
		Object json= JSONObject.toJSON(result);
		System.out.println(json);
	}

}