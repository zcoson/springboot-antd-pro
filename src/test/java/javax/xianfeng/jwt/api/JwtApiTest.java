package javax.xianfeng.jwt.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xianfeng.jwt.model.JwtUser;

import org.junit.Test;

/**
 * @since 2020/01/29 17:15:01
 */
public class JwtApiTest {
	
	/**
	 * eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxMDAxIn0.QyJXhdeq1mGNZQswD57KbGpJDOtTPJ-szyK5mMiHs8Y
	 * 
	 * @since 2020/01/29 17:35:02
	 */
	@Test
	public void getToken() {
		JwtUser user = new JwtUser("1001", "Admin");
		System.out.println(JwtApi.getToken(user));
	}
	
	/**
	 * eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxMDAxIiwiZXhwIjoxNTc3NzIxNjAwfQ.olJVOcSlEsub5Q_YliVNKydJkLhQJwGv8GnAEyzrlzc
	 * 
	 * @throws ParseException
	 * @since 2020/01/29 17:35:18
	 */
	@Test
	public void getTokenWithExpiredTime() throws ParseException {
		JwtUser user = new JwtUser("1001", "Admin");
		Date expiredTime = new SimpleDateFormat("yyyy-MM-dd").parse("2019-12-31");
		System.out.println(JwtApi.getToken(user, expiredTime));
	}
	
}
