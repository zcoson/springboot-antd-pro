package javax.xianfeng.antdpro.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @since 2020/01/27 22:52:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Paging implements Serializable {
	
	private static final long serialVersionUID = -4817662191265183104L;

	private int current;
	
	private int  pageSize;
	
	private long total;
	
}
