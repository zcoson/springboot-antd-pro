package javax.xianfeng.antdpro.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @since 2020/01/27 22:55:18
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ApiResult implements Serializable {

	private static final long serialVersionUID = -6443359539556907029L;

	private Integer code;

	private String msg;
	
	private Object data;
	
	public ApiResult() {
		super();
	}

	public ApiResult(Integer code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}
	
	public ApiResult(Object data) {
		this(ApiResultEnums.SUCCESS.getCode(), ApiResultEnums.SUCCESS.getText());
		this.data = data;
	}
}
