package javax.xianfeng.antdpro.model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @since 2020/01/27 22:48:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class PagingApiResult implements Serializable {
	
	private static final long serialVersionUID = 7540475305220070579L;

	private List<? extends Serializable> list;
	
	private Paging pagination;
	
}
