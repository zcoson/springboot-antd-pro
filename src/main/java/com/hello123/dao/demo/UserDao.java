package com.hello123.dao.demo;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hello123.model.demo.User;

/**
 * @since 2020/01/28 12:39:10
 */
@Repository
public interface UserDao extends CrudRepository<User, Long>, JpaSpecificationExecutor<User> {

	User getById(Long id);

//	 @Query("from User u where u.name = :name")   //SPEL表达式
	Page<User> findAll(Pageable pageable);

	void deleteById(Long id);

	@Modifying
	@Transactional
	@Query("delete from User s where s.id in (?1)")
	void batchDelete(List<Long> ids);

	void deleteByIdIn(List<Long> ids);

	User save(User user);

	@Transactional(rollbackFor = Exception.class)
	@Modifying
	@Query(value = "update t_user set type = :type, name = :name, sex = :sex, birthday = :birthday, age = :age, note = :note where id = :id", nativeQuery = true)
	int update(Long id, String type, String name, Byte sex, Date birthday, Integer age, String note);
}
