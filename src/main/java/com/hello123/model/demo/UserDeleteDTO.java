package com.hello123.model.demo;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @since 2020/01/28 15:08:09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserDeleteDTO {
	
	private List<Long> id;

}
