package com.hello123.model.antdpro;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @since 2020/01/26 23:25:17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class LoginDTO implements Serializable {

	private static final long serialVersionUID = 8921275155818194970L;

	private String userName;
	
	private String password;
	
	private String type;
	
}
